﻿namespace BuzznSchoolEditor
{
    partial class EditQuestionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxCaption = new System.Windows.Forms.GroupBox();
            this.textBoxQuestion = new System.Windows.Forms.TextBox();
            this.textBoxWrongAnswer1 = new System.Windows.Forms.TextBox();
            this.textBoxCorrectAnswer = new System.Windows.Forms.TextBox();
            this.textBoxWrongAnswer2 = new System.Windows.Forms.TextBox();
            this.textBoxWrongAnswer3 = new System.Windows.Forms.TextBox();
            this.groupBoxCorrectAnswer = new System.Windows.Forms.GroupBox();
            this.groupBoxWrongAnswers = new System.Windows.Forms.GroupBox();
            this.groupBoxQuizzes = new System.Windows.Forms.GroupBox();
            this.checkedListBoxQuizzes = new System.Windows.Forms.CheckedListBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxCaption.SuspendLayout();
            this.groupBoxCorrectAnswer.SuspendLayout();
            this.groupBoxWrongAnswers.SuspendLayout();
            this.groupBoxQuizzes.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxCaption
            // 
            this.groupBoxCaption.Controls.Add(this.textBoxQuestion);
            this.groupBoxCaption.Location = new System.Drawing.Point(13, 13);
            this.groupBoxCaption.Name = "groupBoxCaption";
            this.groupBoxCaption.Size = new System.Drawing.Size(319, 54);
            this.groupBoxCaption.TabIndex = 0;
            this.groupBoxCaption.TabStop = false;
            this.groupBoxCaption.Text = "Caption";
            // 
            // textBoxQuestion
            // 
            this.textBoxQuestion.Location = new System.Drawing.Point(6, 19);
            this.textBoxQuestion.Name = "textBoxQuestion";
            this.textBoxQuestion.Size = new System.Drawing.Size(303, 20);
            this.textBoxQuestion.TabIndex = 1;
            // 
            // textBoxWrongAnswer1
            // 
            this.textBoxWrongAnswer1.Location = new System.Drawing.Point(9, 19);
            this.textBoxWrongAnswer1.Name = "textBoxWrongAnswer1";
            this.textBoxWrongAnswer1.Size = new System.Drawing.Size(303, 20);
            this.textBoxWrongAnswer1.TabIndex = 5;
            // 
            // textBoxCorrectAnswer
            // 
            this.textBoxCorrectAnswer.Location = new System.Drawing.Point(9, 19);
            this.textBoxCorrectAnswer.Name = "textBoxCorrectAnswer";
            this.textBoxCorrectAnswer.Size = new System.Drawing.Size(303, 20);
            this.textBoxCorrectAnswer.TabIndex = 3;
            // 
            // textBoxWrongAnswer2
            // 
            this.textBoxWrongAnswer2.Location = new System.Drawing.Point(9, 45);
            this.textBoxWrongAnswer2.Name = "textBoxWrongAnswer2";
            this.textBoxWrongAnswer2.Size = new System.Drawing.Size(303, 20);
            this.textBoxWrongAnswer2.TabIndex = 7;
            // 
            // textBoxWrongAnswer3
            // 
            this.textBoxWrongAnswer3.Location = new System.Drawing.Point(9, 71);
            this.textBoxWrongAnswer3.Name = "textBoxWrongAnswer3";
            this.textBoxWrongAnswer3.Size = new System.Drawing.Size(303, 20);
            this.textBoxWrongAnswer3.TabIndex = 9;
            // 
            // groupBoxCorrectAnswer
            // 
            this.groupBoxCorrectAnswer.Controls.Add(this.textBoxCorrectAnswer);
            this.groupBoxCorrectAnswer.Location = new System.Drawing.Point(13, 74);
            this.groupBoxCorrectAnswer.Name = "groupBoxCorrectAnswer";
            this.groupBoxCorrectAnswer.Size = new System.Drawing.Size(319, 52);
            this.groupBoxCorrectAnswer.TabIndex = 10;
            this.groupBoxCorrectAnswer.TabStop = false;
            this.groupBoxCorrectAnswer.Text = "Correct Answer";
            // 
            // groupBoxWrongAnswers
            // 
            this.groupBoxWrongAnswers.Controls.Add(this.textBoxWrongAnswer3);
            this.groupBoxWrongAnswers.Controls.Add(this.textBoxWrongAnswer2);
            this.groupBoxWrongAnswers.Controls.Add(this.textBoxWrongAnswer1);
            this.groupBoxWrongAnswers.Location = new System.Drawing.Point(13, 133);
            this.groupBoxWrongAnswers.Name = "groupBoxWrongAnswers";
            this.groupBoxWrongAnswers.Size = new System.Drawing.Size(319, 104);
            this.groupBoxWrongAnswers.TabIndex = 11;
            this.groupBoxWrongAnswers.TabStop = false;
            this.groupBoxWrongAnswers.Text = "Wrong Answers";
            // 
            // groupBoxQuizzes
            // 
            this.groupBoxQuizzes.Controls.Add(this.checkedListBoxQuizzes);
            this.groupBoxQuizzes.Location = new System.Drawing.Point(13, 244);
            this.groupBoxQuizzes.Name = "groupBoxQuizzes";
            this.groupBoxQuizzes.Size = new System.Drawing.Size(319, 125);
            this.groupBoxQuizzes.TabIndex = 12;
            this.groupBoxQuizzes.TabStop = false;
            this.groupBoxQuizzes.Text = "Quizzes";
            // 
            // checkedListBoxQuizzes
            // 
            this.checkedListBoxQuizzes.CheckOnClick = true;
            this.checkedListBoxQuizzes.FormattingEnabled = true;
            this.checkedListBoxQuizzes.Location = new System.Drawing.Point(9, 20);
            this.checkedListBoxQuizzes.Name = "checkedListBoxQuizzes";
            this.checkedListBoxQuizzes.Size = new System.Drawing.Size(300, 94);
            this.checkedListBoxQuizzes.Sorted = true;
            this.checkedListBoxQuizzes.TabIndex = 0;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(55, 376);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(101, 28);
            this.buttonSave.TabIndex = 13;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(179, 376);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(106, 28);
            this.buttonCancel.TabIndex = 14;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // EditQuestionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 416);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBoxQuizzes);
            this.Controls.Add(this.groupBoxWrongAnswers);
            this.Controls.Add(this.groupBoxCorrectAnswer);
            this.Controls.Add(this.groupBoxCaption);
            this.Name = "EditQuestionForm";
            this.Text = "Edit Question";
            this.groupBoxCaption.ResumeLayout(false);
            this.groupBoxCaption.PerformLayout();
            this.groupBoxCorrectAnswer.ResumeLayout(false);
            this.groupBoxCorrectAnswer.PerformLayout();
            this.groupBoxWrongAnswers.ResumeLayout(false);
            this.groupBoxWrongAnswers.PerformLayout();
            this.groupBoxQuizzes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxCaption;
        private System.Windows.Forms.TextBox textBoxWrongAnswer1;
        private System.Windows.Forms.TextBox textBoxCorrectAnswer;
        private System.Windows.Forms.TextBox textBoxQuestion;
        private System.Windows.Forms.TextBox textBoxWrongAnswer2;
        private System.Windows.Forms.TextBox textBoxWrongAnswer3;
        private System.Windows.Forms.GroupBox groupBoxCorrectAnswer;
        private System.Windows.Forms.GroupBox groupBoxWrongAnswers;
        private System.Windows.Forms.GroupBox groupBoxQuizzes;
        private System.Windows.Forms.CheckedListBox checkedListBoxQuizzes;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
    }
}