﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using BuzznSchoolServer.Database.Objects;
using Newtonsoft.Json;

namespace BuzznSchoolServer.Database
{
    public class JsonDatabase
    {

        /// <summary>
        /// Enum für Arten von den Daten
        /// </summary>
        public enum Files
        {
            Quiz,
            Questions
        }

        /// <summary>
        /// Datenpfad für die Fragen
        /// </summary>
        private string FilePathQuestions = "";
        /// <summary>
        /// Datenpfad für die Quizzes
        /// </summary>
        private string FilePathQuiz = "";
        /// <summary>
        /// Datenpfad für die Datenbank
        /// </summary>
        private string DatabasePath = "";
        /// <summary>
        /// Variable für den Quiz DB Handler 
        /// </summary>
        private QuizDbHandler _quizInDb = new QuizDbHandler();
        /// <summary>
        /// Atribute für alle Fragen im System
        /// </summary>
        private QuestionDbHandler _questionsInDb = new QuestionDbHandler();

        /// <summary>
        /// Erstellt eine Datenbank
        /// </summary>
        /// <param name="databasePath">Datenbanpfad, default gesetzt JsonDatbase</param>
        public JsonDatabase(String databasePath = "JsonDatabase")
        {
            DatabasePath = databasePath;
            FilePathQuiz = databasePath + "/Quiz";
            FilePathQuestions = databasePath + "/Questions";
            if (!Directory.Exists(DatabasePath))
                Directory.CreateDirectory(DatabasePath);
            _quizInDb = LoadFile<QuizDbHandler>(Files.Quiz);
            _questionsInDb = LoadFile<QuestionDbHandler>(Files.Questions);

        }



        /// <summary>
        /// Speichert alle drei Datensätze
        /// </summary>
        public void SaveAll()
        {
            Save(Files.Quiz);
            Save(Files.Questions);
        }

        /// <summary>
        /// Speichert einen Datensatz
        /// </summary>
        /// <param name="fi">Der Datensatz, der bearbeitet werden soll</param>
        private void Save(Files fi)
        {
            string content;
            string path;
            switch (fi)
            {

                case Files.Quiz:
                    content = JsonConvert.SerializeObject(_quizInDb);
                    path = FilePathQuiz;
                    break;
                case Files.Questions:
                    content = JsonConvert.SerializeObject(_questionsInDb);
                    path = FilePathQuestions;
                    break;
                default:
                    content = JsonConvert.SerializeObject(_questionsInDb);
                    path = FilePathQuiz;
                    break;
            }
            File.Delete(path);
            File.WriteAllText(path, content);
        }

        /// <summary>
        /// Lädt einen Datensatz
        /// </summary>
        /// <param name="fi">Der Datensatz, der geladen werden soll</param>
        /// <typeparam name="T">Die Klasse des Datensatzes</typeparam>
        /// <returns>Gibt den geladen Datensatz wieder</returns>
        private T LoadFile<T>(Files fi)
        {
            var path = fi == Files.Quiz ? FilePathQuiz : FilePathQuestions;
            if (File.Exists(path))
            {
                var contents = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(contents); //TODO there should be something like an try-catch?!
            }
            File.Create(path).Close();
            Save(fi);
            return LoadFile<T>(fi); //file is now there so load dat file!
        }


        /* Interface */

        /// <summary>
        /// Fügt eine Frage hinzu
        /// </summary>
        /// <param name="question">Frage, die hinzugefügt werden soll</param>
        /// <returns>Gibt die Frage zurück</returns>
        public Question AddQuestion(Question question)
        {
            _questionsInDb.AddQuestion(question);
            SaveAll();
            return question;
        }


        /// <summary>
        /// Löscht eine Frage von einem Quiz
        /// </summary>
        /// <param name="question">Frage, die gelöscht werden soll</param>
        /// <param name="quiz">Quiz aus dem gelöscht werden soll</param>
        public void RemoveFromQuiz(Question question, string quiz)
        {
            _quizInDb.DelQuestionfromQuiz(quiz, _questionsInDb.GetIDByReferenz(question));
            SaveAll();
        }

        /// <summary>
        /// fügt eine Frage zu einem Quiz hinzu
        /// </summary>
        /// <param name="question">Question.</param>
        /// <param name="quiz">Quiz.</param>
        public void AddToQuiz(Question question, string quiz)
        {
            _quizInDb.AddQuestionToQuiz(quiz, _questionsInDb.GetIDByReferenz(question));
            SaveAll();
        }

        /// <summary>
        /// fügt ein Quiz hinzu
        /// </summary>
        /// <param name="name">Name.</param>
        public void AddQuiz(String name)
        {
            _quizInDb.AddQuiz(name);
            Save(Files.Quiz);
        }

        /// <summary>
        /// Gibt eine Frage zur passenden ID wieder
        /// </summary>
        /// <param name="id">ID der gesuchten Frage</param>
        /// <returns>Gibt die Frage zurück</returns>
        private Question GetQuestion(int id) => _questionsInDb.GetQuestionById(id);

        /// <summary>
        /// Gibt alle Fragen eines Quiz wieder
        /// </summary>
        /// <param name="quizName">Quiz</param>
        /// <returns>Fragen in einer Liste</returns>
        public List<Question> GetQuiz(string quizName)
        {
            var idList = _quizInDb.GetQuestion(quizName);
            return idList.Select(id => GetQuestion(id)).ToList();
        }

        /// <summary>
        /// Löscht die Frage mit der ID dq
        /// </summary>
        /// <param name="q">Frage, die gelöscht werden soll</param>
        public void DelQuestion(Question q)
        {
            foreach (String s in GetQuizzesNames())
            {
                if (_quizInDb.HasQuizQuestion(s, _questionsInDb.GetIDByReferenz(q)))
                {
                    _quizInDb.DelQuestionfromQuiz(s, _questionsInDb.GetIDByReferenz(q));
                }
            }
            _questionsInDb.DelQuestion(q);

            SaveAll();
        }
        public List<Question> GetAllQuestions()
        {
            return _questionsInDb.GetAllQuestion().Values.ToList();
        }

        public void DelQuiz(String quiz)
        {
            if (!_quizInDb.hasQuiz(quiz))
            {
                return;
            }
            Collection<int> question = _quizInDb.GetQuestion(quiz);
            List<int> toDelete = new List<int>(question);
            for (int i = 0; i < question.Count; i++)
            {
                int quest_ID = question[0];
                foreach (string que in _quizInDb.GetNames())
                {
                    if (_quizInDb.HasQuizQuestion(que, quest_ID))
                    {
                        toDelete.Remove(quest_ID);
                    }
                }
            }
            foreach (int del in toDelete)
            {
                _questionsInDb.questionDir.Remove(del);
            }
            _quizInDb.DelQuiz(quiz);
            SaveAll();
        }

        /// <summary>
        /// Testet, ob eine Frage vorhanden ist.
        /// </summary>
        /// <returns><c>true</c>, if question was hased, <c>false</c> otherwise.</returns>
        /// <param name="id">ID von der Frage </param>
        public bool hasQuestion(Question id)
        {
            return _questionsInDb.hasQuestion(id);
        }

        /// <summary>
        /// Hases the quiz.
        /// </summary>
        /// <returns><c>true</c>, if quiz was hased, <c>false</c> otherwise.</returns>
        /// <param name="quizName">Quiz name.</param>
        public bool hasQuiz(string quizName)
        {
            return _quizInDb.hasQuiz(quizName);
        }

        /// <summary>
        /// Gibt alle Namen der Quizzes wieder
        /// </summary>
        /// <returns>Namen der Quizzes</returns>
        public string[] GetQuizzesNames() => _quizInDb.GetNames();
    }
}
