﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace BuzznSchoolServer.Database.Objects
{
    public class QuizDbHandler
    {
        public Dictionary<string, Collection<int>> QuizDict;

        /// <summary>
        /// Datentyp
        /// </summary>
        public QuizDbHandler()
        {
            QuizDict = new Dictionary<string, Collection<int>>();
        }

        /// <summary>
        /// Gibt die Namen der Quizzes wieder
        /// </summary>
        /// <returns>Namen der Quizzes</returns>
        public string[] GetNames()
        {
            return QuizDict.Keys.ToArray();
        }


        /// <summary>
        /// Gibt alle Fragen aus einem Quiz zurück
        /// </summary>
        /// <returns>Gibt alle Fragen als Collection von int zurück</returns>
        /// <param name="name">Name des Quiz, von dem die Fragen zurück gegeben werden sollen</param>
        public Collection<int> GetQuestion(string name)
        {
            return QuizDict[name];
        }

        /// <summary>
        /// Löscht ein quiz
        /// </summary>
        /// <param name="quiz">Das Quiz, dass gelöscht werden soll</param>
        public void DelQuiz(string quiz)
        {
            QuizDict.Remove(quiz);
        }

        public void AddQuiz(String name)
        {
            QuizDict.Add(name, new Collection<int>());
        }

        /// <summary>
        /// Gibt eine Kopie des Quiz-Datensatz wieder
        /// </summary>
        /// <returns>Kopie des Quizdatensatz</returns>
        public Dictionary<string, Collection<int>> GetQuiz()
        {
            return new Dictionary<string, Collection<int>>(QuizDict);
        }


        /// <summary>
        /// fügt eine Frage zu einem Quiz hinzu
        /// </summary>
        /// <param name="name">Name des Quiz</param>
        /// <param name="question">Die Frage die zu dem Quiz hinzugefügt werden soll</param>
        public void AddQuestionToQuiz(string name, int question)
        {
            if (!QuizDict.ContainsKey(name))
                QuizDict.Add(name, new Collection<int>());
            QuizDict[name].Add(question);
        }

        public void DelQuestionfromQuiz(string name, int question)
        {
            if (QuizDict.ContainsKey(name))
            {
                if (QuizDict[name].Contains(question))
                {
                    QuizDict[name].Remove(question);
                }
            }
        }

        /// <summary>
        /// testet, ob eine Quiz vorhanden ist
        /// </summary>
        /// <returns><c>true</c>, wenn Quiz vorhanden ist, <c>false</c> sonst.</returns>
        /// <param name="quizName">Quiz name.</param>
        public bool hasQuiz(string quizName)
        {
            return this.QuizDict.ContainsKey(quizName);
        }
        /// <summary>
        /// testet, ob ein Quiz eine Frage hat
        /// </summary>
        /// <returns><c>true</c>, wenn ein Quiz eine Frage hat, <c>false</c> sonst.</returns>
        /// <param name="quiz">Quiz.</param>
        /// <param name="question">Frage.</param>
        public bool HasQuizQuestion(String quiz, int question)
        {
            if (QuizDict[quiz].Contains(question))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
