﻿namespace BuzznSchoolServer.Database.Objects
{
    /// <summary>
    /// Datentyp für Fragen
    /// </summary>
    public class Question
    {
        /// <summary>
        /// Erste Antwort
        /// </summary>
        public string answer0;
        /// <summary>
        /// Zweite Antwort
        /// </summary>
        public string answer1;
        /// <summary>
        /// Dritte Antwort
        /// </summary>
        public string answer2;
        /// <summary>
        /// Vierte Antwort
        /// </summary>
        public string answer3;
        /// <summary>
        /// Die Frage
        /// </summary>
        public string question;
        /// <summary>
        /// Die richtige Antwort, 1ste Antwort:0
        /// </summary>
        public int rightAnswer; //0-3

        public override string ToString()
        {
            return question;
        }
    }
}