﻿using System.Collections.Generic;

namespace BuzznSchoolServer.Database.Objects
{
    /// <summary>
    /// Fragenhanlder
    /// </summary>
    public class QuestionDbHandler
    {
        /// <summary>
        /// Dictionary für Fragen
        /// </summary>
        public Dictionary<int, Question> questionDir;

        public QuestionDbHandler()
        {
            questionDir = new Dictionary<int, Question>();
        }

        /// <summary>
        /// Methode, zum fragen hinzufügen
        /// <br> Wenn ein Frage bereits exisitert ist, wird ein Fehler erzeugt beim erneuten Einfügen,
        ///                                      die Frage wird nicht eingefügt </br>
        /// </summary>
        /// <param name="q">Frage, die hinzugefügt werden soll</param>
        /// <param name="doNotRaiseError"><c>true</c> wird keine Fehlermeldung erzeugt
        ///             Frage wird nicht eingefügt <c>false</c>Fehlermeldung wird erzeugt</param>
        /// <returns>Die ID der Frage</returns>
        public int AddQuestion(Question q, bool doNotRaiseError = false)
        {
            if (questionDir.ContainsValue(q))
            {
                throw new System.Exception("Das Objekt ist bereits in der Datenbank, duh!");
            }
            var newQuestionId = questionDir.Count;
            while (questionDir.ContainsKey(newQuestionId))
                newQuestionId = newQuestionId + 1;
            questionDir.Add(newQuestionId, q);
            return newQuestionId;
        }

        /// <summary>
        /// Methode löscht eine Frage
        /// </summary>
        /// <param name="ID">
        /// Die ID zur Frage, die gelöscht werden soll
        /// </param>
        /// <returns>
        /// boolischer Wert, ob löschen erfolgreich war.
        /// </returns>
        public bool DelQuestion(Question q)
        {
            int id = GetIDByReferenz(q);
            if (id != -1)
            {
                return questionDir.Remove(id);
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// gibt eine Frage zurück, die zu der ID gehört.
        /// </summary>
        /// <param name="id">Die ID, die zur ID passt</param>
        /// <returns>Gibt die passende Frage zurück</returns>
        public Question GetQuestionById(int id)
        {
            return questionDir.ContainsKey(id) ? questionDir[id] : null;
        }

        /// <summary>
        /// Gibt die ID einer Frage per Referenz zurück
        /// </summary>
        /// <returns>ID, sollte eine Frage zweimal vorhanden sein, die ID der ersten</returns>
        /// <param name="q">Frage</param>
        public int GetIDByReferenz(Question q)
        {
            if (!questionDir.ContainsValue(q))
            {
                return -1;
            }
            foreach (KeyValuePair<int, Question> kv in questionDir)
            {
                if (q == kv.Value)
                {
                    return kv.Key;
                }
            }
            return -1;
        }

        /// <summary>
        /// gibt alle Fragen zurück
        /// </summary>
        /// <returns>key: ID der Frage, Value: ist die Frage</returns>
        public Dictionary<int, Question> GetAllQuestion()
        {
            return new Dictionary<int, Question>(questionDir); //TODO make this return final
        }

        /// <summary>
        /// Hases the question.
        /// </summary>
        /// <returns><c>true</c>, if question was hased, <c>false</c> otherwise.</returns>
        /// <param name="ID">Identifier.</param>
        private bool hasID(int ID)
        {
            return this.questionDir.ContainsKey(ID);
        }

        /// <summary>
        /// Testet, ob die Frage enhalten ist
        /// </summary>
        /// <returns><c>true</c>, wenn die Frage vorhanden ist, <c>false</c> sonst.</returns>
        /// <param name="q">Q.</param>
        public bool hasQuestion(Question q)
        {
            return questionDir.ContainsValue(q);
        }

    }
}
