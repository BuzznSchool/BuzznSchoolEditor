﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using BuzznSchoolServer.Database.Objects;

namespace BuzznSchoolEditor
{
    public partial class AddQuestionForm : BuzznSchoolEditor.EditQuestionForm
    {
        MainForm daddy;
        Question question;

        public AddQuestionForm(MainForm daddy, Question question) : base(daddy, question)
        {
            this.daddy = daddy;
            this.question = question;
            InitializeComponent();
            UpdateQuizzes();
        }

        protected override void buttonSave_Click(object sender, EventArgs e)
        {
            daddy.jdb.AddQuestion(question);
            SaveAndClose("The question has been successfully added!", "Question added");
        }
    }
}
