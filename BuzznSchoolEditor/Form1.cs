﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BuzznSchoolServer.Database;
using BuzznSchoolServer.Database.Objects;

namespace BuzznSchoolEditor
{
    public partial class MainForm : Form
    {
        public JsonDatabase jdb { get; set; }

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            jdb = new JsonDatabase();

            UpdateScreen();
        }

        public void UpdateScreen()
        {
            comboBoxQuizzes.Items.Clear();
            string[] allQuizzes = jdb.GetQuizzesNames();

            if (allQuizzes.Length == 0)
            {
                comboBoxQuizzes.Items.Add("-= No Quizzes =-");
                comboBoxQuizzes.SelectedIndex = 0;
                comboBoxQuizzes.Enabled = false;
                buttonRemoveQuiz.Enabled = false;
            }
            else
            {
                comboBoxQuizzes.Enabled = true;
                buttonRemoveQuiz.Enabled = true;
                comboBoxQuizzes.Items.AddRange(allQuizzes);
                comboBoxQuizzes.SelectedIndex = 0;
            }

            UpdateQuestions(checkBoxShowAllQuestions.Checked);
        }

        private void UpdateQuestions(bool bShowAll)
        {
            comboBoxQuestions.Items.Clear();
            comboBoxQuestions.Enabled = true;
            buttonEditQuestion.Enabled = true;
            buttonRemoveQuestion.Enabled = true;

            // Show questions for quiz
            if (comboBoxQuizzes.Enabled && !bShowAll)
            {
                Question[] questions;
                try
                {
                    questions = jdb.GetQuiz(comboBoxQuizzes.Text).ToArray();
                }
                catch (Exception ex)
                {
                    questions = null;
                }

                if (questions != null && questions.Length > 0)
                {
                    comboBoxQuestions.Items.AddRange(questions);
                    comboBoxQuestions.SelectedIndex = 0;

                    return;
                }
            }
            
            else
            {
                Question[] questions = jdb.GetAllQuestions().ToArray();

                if (questions != null && questions.Length > 0)
                {
                    comboBoxQuestions.Items.AddRange(questions);
                    comboBoxQuestions.SelectedIndex = 0;

                    return;
                }
            }

            // No questions? -> Tell the user
            comboBoxQuestions.Items.Add("-= No Questions =-");
            comboBoxQuestions.SelectedIndex = 0;
            comboBoxQuestions.Enabled = false;
            buttonEditQuestion.Enabled = false;
            buttonRemoveQuestion.Enabled = false;
        }

        private void buttonAddQuiz_Click(object sender, EventArgs e)
        {
            (new AddQuizForm(this)).Show();
        }

        private void buttonAddQuestion_Click(object sender, EventArgs e)
        {
            (new AddQuestionForm(this, new Question())).Show();
        }

        private void comboBoxQuizzes_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateQuestions(checkBoxShowAllQuestions.Checked);
        }

        private void checkBoxShowAllQuestions_CheckedChanged(object sender, EventArgs e)
        {
            UpdateQuestions(checkBoxShowAllQuestions.Checked);
        }

        private void buttonEditQuestion_Click(object sender, EventArgs e)
        {
            Question question = comboBoxQuestions.SelectedItem as Question;
            (new EditQuestionForm(this, question)).Show();
        }

        private void buttonRemoveQuiz_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to remove the quiz '" + comboBoxQuizzes.Text + "'?", "Remove Quiz", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                jdb.DelQuiz(comboBoxQuizzes.Text);
                UpdateScreen();
                MessageBox.Show("The quiz was successfully removed!", "Quiz Removed");
            }
        }

        private void buttonRemoveQuestion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to remove the question?", "Remove Question", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                jdb.DelQuestion(comboBoxQuestions.SelectedItem as Question);
                UpdateScreen();
                MessageBox.Show("The question was successfully removed!", "Question Removed");
            }
        }
    }
}
