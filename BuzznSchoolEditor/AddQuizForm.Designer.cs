﻿namespace BuzznSchoolEditor
{
    partial class AddQuizForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.groupBoxName = new System.Windows.Forms.GroupBox();
            this.buttonAddQuiz = new System.Windows.Forms.Button();
            this.groupBoxName.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(6, 19);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(307, 20);
            this.textBoxName.TabIndex = 0;
            this.textBoxName.Text = "New Quiz";
            // 
            // groupBoxName
            // 
            this.groupBoxName.Controls.Add(this.textBoxName);
            this.groupBoxName.Location = new System.Drawing.Point(13, 4);
            this.groupBoxName.Name = "groupBoxName";
            this.groupBoxName.Size = new System.Drawing.Size(319, 53);
            this.groupBoxName.TabIndex = 1;
            this.groupBoxName.TabStop = false;
            this.groupBoxName.Text = "Name";
            // 
            // buttonAddQuiz
            // 
            this.buttonAddQuiz.Location = new System.Drawing.Point(117, 63);
            this.buttonAddQuiz.Name = "buttonAddQuiz";
            this.buttonAddQuiz.Size = new System.Drawing.Size(105, 29);
            this.buttonAddQuiz.TabIndex = 2;
            this.buttonAddQuiz.Text = "Add Quiz";
            this.buttonAddQuiz.UseVisualStyleBackColor = true;
            this.buttonAddQuiz.Click += new System.EventHandler(this.buttonAddQuizz_Click);
            // 
            // AddQuizForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 99);
            this.Controls.Add(this.buttonAddQuiz);
            this.Controls.Add(this.groupBoxName);
            this.Name = "AddQuizForm";
            this.Text = "Add Quiz";
            this.groupBoxName.ResumeLayout(false);
            this.groupBoxName.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.GroupBox groupBoxName;
        private System.Windows.Forms.Button buttonAddQuiz;
    }
}