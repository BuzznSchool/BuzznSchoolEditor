﻿namespace BuzznSchoolEditor
{
    partial class AddQuestionForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBoxQuizzes = new System.Windows.Forms.GroupBox();
            this.checkedListBoxQuizzes = new System.Windows.Forms.CheckedListBox();
            this.groupBoxWrongAnswers = new System.Windows.Forms.GroupBox();
            this.textBoxWrongAnswer3 = new System.Windows.Forms.TextBox();
            this.textBoxWrongAnswer2 = new System.Windows.Forms.TextBox();
            this.textBoxWrongAnswer1 = new System.Windows.Forms.TextBox();
            this.groupBoxCorrectAnswer = new System.Windows.Forms.GroupBox();
            this.textBoxCorrectAnswer = new System.Windows.Forms.TextBox();
            this.groupBoxCaption = new System.Windows.Forms.GroupBox();
            this.textBoxQuestion = new System.Windows.Forms.TextBox();
            this.groupBoxQuizzes.SuspendLayout();
            this.groupBoxWrongAnswers.SuspendLayout();
            this.groupBoxCorrectAnswer.SuspendLayout();
            this.groupBoxCaption.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(178, 375);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(106, 28);
            this.buttonCancel.TabIndex = 20;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(54, 375);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(101, 28);
            this.buttonSave.TabIndex = 19;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // groupBoxQuizzes
            // 
            this.groupBoxQuizzes.Controls.Add(this.checkedListBoxQuizzes);
            this.groupBoxQuizzes.Location = new System.Drawing.Point(12, 243);
            this.groupBoxQuizzes.Name = "groupBoxQuizzes";
            this.groupBoxQuizzes.Size = new System.Drawing.Size(319, 125);
            this.groupBoxQuizzes.TabIndex = 18;
            this.groupBoxQuizzes.TabStop = false;
            this.groupBoxQuizzes.Text = "Quizzes";
            // 
            // checkedListBoxQuizzes
            // 
            this.checkedListBoxQuizzes.FormattingEnabled = true;
            this.checkedListBoxQuizzes.Location = new System.Drawing.Point(9, 20);
            this.checkedListBoxQuizzes.Name = "checkedListBoxQuizzes";
            this.checkedListBoxQuizzes.Size = new System.Drawing.Size(300, 94);
            this.checkedListBoxQuizzes.Sorted = true;
            this.checkedListBoxQuizzes.TabIndex = 0;
            // 
            // groupBoxWrongAnswers
            // 
            this.groupBoxWrongAnswers.Controls.Add(this.textBoxWrongAnswer3);
            this.groupBoxWrongAnswers.Controls.Add(this.textBoxWrongAnswer2);
            this.groupBoxWrongAnswers.Controls.Add(this.textBoxWrongAnswer1);
            this.groupBoxWrongAnswers.Location = new System.Drawing.Point(12, 132);
            this.groupBoxWrongAnswers.Name = "groupBoxWrongAnswers";
            this.groupBoxWrongAnswers.Size = new System.Drawing.Size(319, 104);
            this.groupBoxWrongAnswers.TabIndex = 17;
            this.groupBoxWrongAnswers.TabStop = false;
            this.groupBoxWrongAnswers.Text = "Wrong Answers";
            // 
            // textBoxWrongAnswer3
            // 
            this.textBoxWrongAnswer3.Location = new System.Drawing.Point(9, 71);
            this.textBoxWrongAnswer3.Name = "textBoxWrongAnswer3";
            this.textBoxWrongAnswer3.Size = new System.Drawing.Size(303, 20);
            this.textBoxWrongAnswer3.TabIndex = 9;
            // 
            // textBoxWrongAnswer2
            // 
            this.textBoxWrongAnswer2.Location = new System.Drawing.Point(9, 45);
            this.textBoxWrongAnswer2.Name = "textBoxWrongAnswer2";
            this.textBoxWrongAnswer2.Size = new System.Drawing.Size(303, 20);
            this.textBoxWrongAnswer2.TabIndex = 7;
            // 
            // textBoxWrongAnswer1
            // 
            this.textBoxWrongAnswer1.Location = new System.Drawing.Point(9, 19);
            this.textBoxWrongAnswer1.Name = "textBoxWrongAnswer1";
            this.textBoxWrongAnswer1.Size = new System.Drawing.Size(303, 20);
            this.textBoxWrongAnswer1.TabIndex = 5;
            // 
            // groupBoxCorrectAnswer
            // 
            this.groupBoxCorrectAnswer.Controls.Add(this.textBoxCorrectAnswer);
            this.groupBoxCorrectAnswer.Location = new System.Drawing.Point(12, 73);
            this.groupBoxCorrectAnswer.Name = "groupBoxCorrectAnswer";
            this.groupBoxCorrectAnswer.Size = new System.Drawing.Size(319, 52);
            this.groupBoxCorrectAnswer.TabIndex = 16;
            this.groupBoxCorrectAnswer.TabStop = false;
            this.groupBoxCorrectAnswer.Text = "Correct Answer";
            // 
            // textBoxCorrectAnswer
            // 
            this.textBoxCorrectAnswer.Location = new System.Drawing.Point(9, 19);
            this.textBoxCorrectAnswer.Name = "textBoxCorrectAnswer";
            this.textBoxCorrectAnswer.Size = new System.Drawing.Size(303, 20);
            this.textBoxCorrectAnswer.TabIndex = 3;
            // 
            // groupBoxCaption
            // 
            this.groupBoxCaption.Controls.Add(this.textBoxQuestion);
            this.groupBoxCaption.Location = new System.Drawing.Point(12, 12);
            this.groupBoxCaption.Name = "groupBoxCaption";
            this.groupBoxCaption.Size = new System.Drawing.Size(319, 54);
            this.groupBoxCaption.TabIndex = 15;
            this.groupBoxCaption.TabStop = false;
            this.groupBoxCaption.Text = "Caption";
            // 
            // textBoxQuestion
            // 
            this.textBoxQuestion.Location = new System.Drawing.Point(6, 19);
            this.textBoxQuestion.Name = "textBoxQuestion";
            this.textBoxQuestion.Size = new System.Drawing.Size(303, 20);
            this.textBoxQuestion.TabIndex = 1;
            // 
            // AddQuestionForm
            // 
            this.ClientSize = new System.Drawing.Size(344, 411);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBoxQuizzes);
            this.Controls.Add(this.groupBoxWrongAnswers);
            this.Controls.Add(this.groupBoxCorrectAnswer);
            this.Controls.Add(this.groupBoxCaption);
            this.Name = "AddQuestionForm";
            this.Text = "Add Question";
            this.groupBoxQuizzes.ResumeLayout(false);
            this.groupBoxWrongAnswers.ResumeLayout(false);
            this.groupBoxWrongAnswers.PerformLayout();
            this.groupBoxCorrectAnswer.ResumeLayout(false);
            this.groupBoxCorrectAnswer.PerformLayout();
            this.groupBoxCaption.ResumeLayout(false);
            this.groupBoxCaption.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.GroupBox groupBoxQuizzes;
        private System.Windows.Forms.CheckedListBox checkedListBoxQuizzes;
        private System.Windows.Forms.GroupBox groupBoxWrongAnswers;
        private System.Windows.Forms.TextBox textBoxWrongAnswer3;
        private System.Windows.Forms.TextBox textBoxWrongAnswer2;
        private System.Windows.Forms.TextBox textBoxWrongAnswer1;
        private System.Windows.Forms.GroupBox groupBoxCorrectAnswer;
        private System.Windows.Forms.TextBox textBoxCorrectAnswer;
        private System.Windows.Forms.GroupBox groupBoxCaption;
        private System.Windows.Forms.TextBox textBoxQuestion;
    }
}
