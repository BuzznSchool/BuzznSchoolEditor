﻿namespace BuzznSchoolEditor
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxQuizzes = new System.Windows.Forms.ComboBox();
            this.groupBoxQuiz = new System.Windows.Forms.GroupBox();
            this.buttonRemoveQuiz = new System.Windows.Forms.Button();
            this.buttonAddQuiz = new System.Windows.Forms.Button();
            this.buttonRemoveQuestion = new System.Windows.Forms.Button();
            this.buttonEditQuestion = new System.Windows.Forms.Button();
            this.comboBoxQuestions = new System.Windows.Forms.ComboBox();
            this.buttonAddQuestion = new System.Windows.Forms.Button();
            this.groupBoxQuestion = new System.Windows.Forms.GroupBox();
            this.checkBoxShowAllQuestions = new System.Windows.Forms.CheckBox();
            this.groupBoxQuiz.SuspendLayout();
            this.groupBoxQuestion.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxQuizzes
            // 
            this.comboBoxQuizzes.FormattingEnabled = true;
            this.comboBoxQuizzes.Location = new System.Drawing.Point(6, 19);
            this.comboBoxQuizzes.Name = "comboBoxQuizzes";
            this.comboBoxQuizzes.Size = new System.Drawing.Size(302, 21);
            this.comboBoxQuizzes.Sorted = true;
            this.comboBoxQuizzes.TabIndex = 0;
            this.comboBoxQuizzes.SelectedIndexChanged += new System.EventHandler(this.comboBoxQuizzes_SelectedIndexChanged);
            // 
            // groupBoxQuiz
            // 
            this.groupBoxQuiz.Controls.Add(this.buttonRemoveQuiz);
            this.groupBoxQuiz.Controls.Add(this.buttonAddQuiz);
            this.groupBoxQuiz.Controls.Add(this.comboBoxQuizzes);
            this.groupBoxQuiz.Location = new System.Drawing.Point(12, 12);
            this.groupBoxQuiz.Name = "groupBoxQuiz";
            this.groupBoxQuiz.Size = new System.Drawing.Size(320, 89);
            this.groupBoxQuiz.TabIndex = 1;
            this.groupBoxQuiz.TabStop = false;
            this.groupBoxQuiz.Text = "Quiz";
            // 
            // buttonRemoveQuiz
            // 
            this.buttonRemoveQuiz.Location = new System.Drawing.Point(155, 44);
            this.buttonRemoveQuiz.Name = "buttonRemoveQuiz";
            this.buttonRemoveQuiz.Size = new System.Drawing.Size(153, 36);
            this.buttonRemoveQuiz.TabIndex = 6;
            this.buttonRemoveQuiz.Text = "Remove Quiz";
            this.buttonRemoveQuiz.UseVisualStyleBackColor = true;
            this.buttonRemoveQuiz.Click += new System.EventHandler(this.buttonRemoveQuiz_Click);
            // 
            // buttonAddQuiz
            // 
            this.buttonAddQuiz.Location = new System.Drawing.Point(6, 44);
            this.buttonAddQuiz.Name = "buttonAddQuiz";
            this.buttonAddQuiz.Size = new System.Drawing.Size(143, 36);
            this.buttonAddQuiz.TabIndex = 2;
            this.buttonAddQuiz.Text = "Add Quiz";
            this.buttonAddQuiz.UseVisualStyleBackColor = true;
            this.buttonAddQuiz.Click += new System.EventHandler(this.buttonAddQuiz_Click);
            // 
            // buttonRemoveQuestion
            // 
            this.buttonRemoveQuestion.Location = new System.Drawing.Point(155, 70);
            this.buttonRemoveQuestion.Name = "buttonRemoveQuestion";
            this.buttonRemoveQuestion.Size = new System.Drawing.Size(153, 36);
            this.buttonRemoveQuestion.TabIndex = 4;
            this.buttonRemoveQuestion.Text = "Remove Question";
            this.buttonRemoveQuestion.UseVisualStyleBackColor = true;
            this.buttonRemoveQuestion.Click += new System.EventHandler(this.buttonRemoveQuestion_Click);
            // 
            // buttonEditQuestion
            // 
            this.buttonEditQuestion.Location = new System.Drawing.Point(6, 70);
            this.buttonEditQuestion.Name = "buttonEditQuestion";
            this.buttonEditQuestion.Size = new System.Drawing.Size(143, 36);
            this.buttonEditQuestion.TabIndex = 5;
            this.buttonEditQuestion.Text = "Edit Question";
            this.buttonEditQuestion.UseVisualStyleBackColor = true;
            this.buttonEditQuestion.Click += new System.EventHandler(this.buttonEditQuestion_Click);
            // 
            // comboBoxQuestions
            // 
            this.comboBoxQuestions.FormattingEnabled = true;
            this.comboBoxQuestions.Location = new System.Drawing.Point(6, 19);
            this.comboBoxQuestions.Name = "comboBoxQuestions";
            this.comboBoxQuestions.Size = new System.Drawing.Size(302, 21);
            this.comboBoxQuestions.Sorted = true;
            this.comboBoxQuestions.TabIndex = 1;
            // 
            // buttonAddQuestion
            // 
            this.buttonAddQuestion.Location = new System.Drawing.Point(93, 226);
            this.buttonAddQuestion.Name = "buttonAddQuestion";
            this.buttonAddQuestion.Size = new System.Drawing.Size(143, 36);
            this.buttonAddQuestion.TabIndex = 3;
            this.buttonAddQuestion.Text = "Add Question";
            this.buttonAddQuestion.UseVisualStyleBackColor = true;
            this.buttonAddQuestion.Click += new System.EventHandler(this.buttonAddQuestion_Click);
            // 
            // groupBoxQuestion
            // 
            this.groupBoxQuestion.Controls.Add(this.checkBoxShowAllQuestions);
            this.groupBoxQuestion.Controls.Add(this.comboBoxQuestions);
            this.groupBoxQuestion.Controls.Add(this.buttonEditQuestion);
            this.groupBoxQuestion.Controls.Add(this.buttonRemoveQuestion);
            this.groupBoxQuestion.Location = new System.Drawing.Point(12, 108);
            this.groupBoxQuestion.Name = "groupBoxQuestion";
            this.groupBoxQuestion.Size = new System.Drawing.Size(320, 112);
            this.groupBoxQuestion.TabIndex = 6;
            this.groupBoxQuestion.TabStop = false;
            this.groupBoxQuestion.Text = "Question";
            // 
            // checkBoxShowAllQuestions
            // 
            this.checkBoxShowAllQuestions.AutoSize = true;
            this.checkBoxShowAllQuestions.Location = new System.Drawing.Point(6, 47);
            this.checkBoxShowAllQuestions.Name = "checkBoxShowAllQuestions";
            this.checkBoxShowAllQuestions.Size = new System.Drawing.Size(117, 17);
            this.checkBoxShowAllQuestions.TabIndex = 6;
            this.checkBoxShowAllQuestions.Text = "Show All Questions";
            this.checkBoxShowAllQuestions.UseVisualStyleBackColor = true;
            this.checkBoxShowAllQuestions.CheckedChanged += new System.EventHandler(this.checkBoxShowAllQuestions_CheckedChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 271);
            this.Controls.Add(this.groupBoxQuestion);
            this.Controls.Add(this.buttonAddQuestion);
            this.Controls.Add(this.groupBoxQuiz);
            this.Name = "MainForm";
            this.Text = "BuzznSchoolEditor v1.3.3.7";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxQuiz.ResumeLayout(false);
            this.groupBoxQuestion.ResumeLayout(false);
            this.groupBoxQuestion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxQuizzes;
        private System.Windows.Forms.GroupBox groupBoxQuiz;
        private System.Windows.Forms.Button buttonRemoveQuiz;
        private System.Windows.Forms.Button buttonRemoveQuestion;
        private System.Windows.Forms.Button buttonEditQuestion;
        private System.Windows.Forms.ComboBox comboBoxQuestions;
        private System.Windows.Forms.Button buttonAddQuestion;
        private System.Windows.Forms.Button buttonAddQuiz;
        private System.Windows.Forms.GroupBox groupBoxQuestion;
        private System.Windows.Forms.CheckBox checkBoxShowAllQuestions;
    }
}

