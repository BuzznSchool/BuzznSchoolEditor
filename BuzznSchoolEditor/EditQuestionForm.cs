﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BuzznSchoolServer.Database.Objects;

namespace BuzznSchoolEditor
{
    public partial class EditQuestionForm : Form
    {
        MainForm daddy;
        Question question;

        private Random rnd = new Random();

        private int oldRightAnswer;

        private EditQuestionForm() { }

        public EditQuestionForm(MainForm daddy, Question question)
        {
            this.daddy = daddy;
            this.question = question;
            InitializeComponent();
            UpdateScreen();
        }

        protected void UpdateScreen()
        {
            textBoxQuestion.Text = question.question;

            string[] answers = new string[4];
            answers[0] = question.answer0;
            answers[1] = question.answer1;
            answers[2] = question.answer2;
            answers[3] = question.answer3;

            textBoxCorrectAnswer.Text = answers[question.rightAnswer];

            // Set right answer as first
            string t = answers[question.rightAnswer];
            answers[question.rightAnswer] = answers[0];
            answers[0] = t;
            oldRightAnswer = question.rightAnswer;
            question.rightAnswer = 0;

            textBoxWrongAnswer1.Text = answers[1];
            textBoxWrongAnswer2.Text = answers[2];
            textBoxWrongAnswer3.Text = answers[3];

            UpdateQuizzes();
        }

        protected void UpdateQuizzes()
        {
            checkedListBoxQuizzes.Items.Clear();
            string[] allQuizzes = daddy.jdb.GetQuizzesNames();

            foreach (string quiz in allQuizzes)
            {
                checkedListBoxQuizzes.Items.Add(quiz, daddy.jdb.GetQuiz(quiz).Contains(question));
            }
        }

        protected virtual void buttonSave_Click(object sender, EventArgs e)
        {
            SaveAndClose("The question has been successfully edited!", "Question edited");
        }

        protected void SaveAndClose(string text, string caption)
        {
            SaveAndClose();
            MessageBox.Show(text, caption);
        }

        protected void SaveAndClose()
        {
            SaveToQuestion();
            AddToQuizzes();
            daddy.jdb.SaveAll();
            Close();
            daddy.UpdateScreen();
        }

        protected void SaveToQuestion()
        {
            question.question = textBoxQuestion.Text;

            string[] answers = new string[4];
            answers[0] = textBoxCorrectAnswer.Text;
            answers[1] = textBoxWrongAnswer1.Text;
            answers[2] = textBoxWrongAnswer2.Text;
            answers[3] = textBoxWrongAnswer3.Text;

            string[] rndAnswers = answers.OrderBy(x => rnd.Next()).ToArray();

            question.rightAnswer = Array.IndexOf(rndAnswers, textBoxCorrectAnswer.Text);

            question.answer0 = rndAnswers[0];
            question.answer1 = rndAnswers[1];
            question.answer2 = rndAnswers[2];
            question.answer3 = rndAnswers[3];
        }

        protected void AddToQuizzes()
        {
            foreach (string quiz in checkedListBoxQuizzes.Items)
            {
                daddy.jdb.RemoveFromQuiz(question, quiz);
            }

            foreach (string quiz in checkedListBoxQuizzes.CheckedItems)
            {
                daddy.jdb.AddToQuiz(question, quiz);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            question.rightAnswer = oldRightAnswer;
            Close();
        }
    }
}
