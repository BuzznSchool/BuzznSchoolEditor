﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuzznSchoolEditor
{
    public partial class AddQuizForm : Form
    {
        private MainForm daddy;

        public AddQuizForm(MainForm daddy)
        {
            this.daddy = daddy;
            InitializeComponent();
        }

        private void buttonAddQuizz_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(textBoxName.Text))
            {
                MessageBox.Show("Please add a name to the quizz!", "No Name");
                return;
            }

            daddy.jdb.AddQuiz(textBoxName.Text);
            Close();
            daddy.UpdateScreen();
        }
    }
}
